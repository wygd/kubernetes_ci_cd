package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/Pallinder/go-randomdata"
)

type Employee struct {
    Id   int    `json:"id"`
    Name string `json:"name"`
}

var nombre_servidor = "default"
var mensaje = "Mensaje default"

//PruebaHandler funcion
func PruebaHandler (w http.ResponseWriter, request *http.Request) {
	enableCors(&w)

	w.Write([]byte("Yo no hago nada, pero soy accesible desde el ingress y me modificaron"))
}

//MensajeHandler funcion
func MensajeHandler (w http.ResponseWriter, request *http.Request) {
	enableCors(&w)

	w.Write([]byte(mensaje))
}


func main(){
	router := mux.NewRouter()


	router.HandleFunc("/url_prueba", PruebaHandler).Methods("GET")
	router.HandleFunc("/mensaje",MensajeHandler).Methods("GET")
	nombre_servidor = randomdata.SillyName()
	mensaje = "Hola! te saluda el servidor " + nombre_servidor
	log.Println("Estoy escuchando en el puerto 5000")
	
	
	
	
	log.Fatal(http.ListenAndServe(":5000", router))
	
	

}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
