module gitlab.com/m

go 1.17

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/gorilla/mux v1.8.0
)

require golang.org/x/text v0.3.7 // indirect
