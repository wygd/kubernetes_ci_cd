# Kubernetes_CI_CD

## Agregar permisos al usuario con el que administramos el cluster (solo para GKE)
Este comando tambien nos ayudara a tener los permisos necesarios para crear el Ingress Controller

`kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole cluster-admin \
  --user $(gcloud config get-value account)`

## Llenado de información del cluster

### Kubernetes cluster name

### Environment scope

### API URL
Para optener el API URL ejecutamos el siguiente comando en una consola donde tengamos acceso al kluster de kubernetes:

`kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}'`

### CA Certificate
Los pasos para obtener este campo son los siguientes:

1. Listar los secrets con el comando `kubectl get secrets`
2. Copiamos el nombre del elemento cuyo nombre tenga el prefico _default-token_ 
3. Sustituimos el nombre devuelto en el siguiente comando y lo ejecutamos:
`kubectl get secret <nombre del secret devuelto> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`
4. Procedemos a copiar la salida y a pegarla en el campo de CA Certificate.



### Service Token
Creamos un archivo llamado , y dentro colocamos lo siguiente:

``` yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system 
```



Ejecutamos el siguiente comando, el cual aplica los cambios al cluster:

`kubectl apply -f gitlab-admin-service-account.yaml`

Ahora retornamos el token del usuario creado:

`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')`

Copiamos el valor de la salida del token y procedemos a agregarlo al campo token


### Project namespace prefix (optional, unique)
Aqui agremaos el prefijo que deseamos que gitlab agregue a cada namespace que se cree. Un namespace agrupa un conjunto de elementos(pods, servicios, Ingress) y los aisla de otros elementos que se encuentren en distintos namespace.

## Instalación de Ingress Controler
`helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx -n <namespace del enviroment>`